module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		connect: {
			mysite: {
				options: {
					hostname: '127.0.0.1',
					port: 8080,
					protocol: 'http',
					base: 'build/dev',
					livereload: true
				}
			}
		},
		sass: {
			dist: {
				options: {
					style: 'compressed'
				},
				files: {
					'themes/farma/static/css/main.css': 'themes/**/**/main.scss'
				}
			},
			dev: {
				options: {
					style: 'uncompressed'
				},
				files: {
					'themes/farma/static/css/main.css': 'themes/**/**/main.scss'
				}
			}
		},
		autoprefixer: {
			dist: {
				files: {
					'build/dist/css/main.css': 'build/dist/css/main.css'
				}
			},
			dev: {
				files: {
					'build/dev/css/main.css': 'build/dev/css/main.css'
				}
			}
		},
		frontmatter: {
			release: {
				options: {
					minify: false,
					width: '3s',
					event: ['added', 'deleted']
				},
				files: {
					'static/drugs.json': ['content/drug/*.md']
				}
			}
		},
		watch: {
			options: {
				atBegin: true,
				livereload: true
			},
			sass: {
				files: ['themes/**/*.scss'],
				tasks: ['sass:dev', 'autoprefixer:dev']
			},
			frontmatter: {
				files: ['content/**'],
				tasks: 'frontmatter'
			},
			hugo: {
				files: [
					'content/**',
					'data/**',
					'layouts/**',
					'static/**',
					'themes/**/*.html'
				],
				tasks: 'hugo:dev'
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-frontmatter');
	grunt.loadNpmTasks('grunt-autoprefixer');

	grunt.registerTask('dev', [
		'sass:dev',
		'autoprefixer:dev',
		'frontmatter',
		'hugo:dev'
	]);

	grunt.registerTask('default', [
		'sass:dist',
		'autoprefixer:dist',
		'frontmatter',
		'hugo:dist'
	]);

	grunt.registerTask('edit', ['connect', 'watch']);

	/* The following code registers a task that runs hugo to compile the site
     directory, placing the rendered result in either build/dev or build/dist
    (for development and deployment builds respectively). The target is chosen
    by referring to the task as hugo:dev or hugo:dist.
  */
	grunt.registerTask('hugo', function(target) {
		var args, done, e, hugo, i, len, ref, results;
		done = this.async();
		args = ['--source=.', '--destination=build/' + target];
		if (target === 'dev') {
			args.push('--baseUrl=http://127.0.0.1:8080/');
			args.push('--buildDrafts=true');
			args.push('--buildFuture=true');
		}
		hugo = require('child_process').spawn('hugo', args, {
			stdio: 'inherit'
		});
		ref = ['exit', 'error'];
		results = [];
		for (i = 0, len = ref.length; i < len; i++) {
			e = ref[i];
			results.push(
				hugo.on(e, function() {
					return done(true);
				})
			);
		}
		return results;
	});
};
