# Farma.Hugo

## Brugsanvisning
For lokal udvikling, kør

`grunt edit`

For at compile til web-udgaven:

```grunt```

## Forstå dette repo
Siden er bygget med [Hugo](http://gohugo.io), der er en static site generator.

De enkelte stoffers tekst og metadata (kapitel, fed-skrift etc.) findes i `.md`-filer under `content/`. Teksten er skrevet i [Markdown](https://en.wikipedia.org/wiki/Markdown) og metadata i [YAML](https://en.wikipedia.org/wiki/YAML).

Hugo læser det indhold, der findes i `.md`-filer under `content/` og skaber så `.html`-filer derfra ud fra det tema, jeg har skabet i mappen `themes/farma/`. Ud fra denne blanding af råt indhold og et tema spytter Hugo den færdige hjemmeside.

Til slut kan nævnes, at jeg benytter [Grunt](http://gruntjs.com) til at lave en `.json`-fil (`static/drugs.json`) med en liste over alle stoffer og hvilke(t) kapitel(/kapitler) hvert stof hører under. Dette benyttes til søge- og flashcard-funktionerne.

Det meste af sidens funktionalitet er egentlig sikret via javascript -- det er fx herigennem, at de enkelte stoffers info hentes, når der klikkes på dem i stoflisten.

Den endelige hjemmeside findes under `build/`. Mappen `build/dev/` er til lokal udvikling (sidens URL er localhost:8080), mens mappen `build/dist/` er live-udgaven (sidens URL er farma.morsby.dk). Ellers er de to udgaver ens.

## Benyttede pakker, frameworks, programmer etc.
- [MaterializeCSS](http://materializecss.com)
- [Hugo](http://gohugo.io)
- [Grunt](http://gruntjs.com) (de enkelte pakker kan ses i `package.json` under `devDependencies`)
- [Atom](http://atom.io)

## Planlagte mål

- [x] Fix basal brug
- [x] Fix JavaScript-knapper
- [x] Tilføj Piwik-statistik; single-page app ([Piwik](http://developer.piwik.org/guides/tracking-javascript-guide), [StackExchange](http://webmasters.stackexchange.com/questions/28260/analytics-for-single-page-javascript-webapps-google-analytics)).
- [x] Mellemrum for "næste/vis" på Flashcards
- [x] Filtrering.
- [x] Skift til online Jquery.
- [x] Fiks sortering!
