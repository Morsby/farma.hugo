var latestDrugs = [0,0,0];

var randomCard = function(arr, chapters) {

    if(chapters == "0") {
        return;
    }

    var randomNumber = Math.floor((Math.random() * arr.length) + 0);

    var randomDrug = arr[randomNumber];

    var matchingChapters = false;

    for (i = 0; i < randomDrug.chapters.length; i++) {
        if(randomDrug.chapters[i] != null) {
            thisChap = randomDrug.chapters[i].toString();
        } else {
            thisChap = "0";
        }
        if ($.inArray(thisChap, chapters) != -1) {
            matchingChapters = true;
        }
    }


    if (matchingChapters == true && !($.inArray(randomDrug.slug,latestDrugs.slice(-3)) != -1)) {
        latestDrugs.push(randomDrug.slug);
        getFlashcard(randomDrug);
    } else {
        randomCard(arr, chapters)
    }
}

var getFlashcard = function(drug) {
    $.get("/drug/" + drug.slug + ".html", function(data) {
        data = $.parseHTML(data);
        $(data).find(".drug-hide").html("Vis info [–]")
        $(data).find(".drug-close").html("Ny [»]")
        $(data).prependTo("#main-drugs").slideDown();
    });
};


var getCheckedChapters = function() {
    var checkedChapters = $("input.filter-chapter:checkbox:checked").map(function() {
        return $(this).val();
    }).get();

    if(checkedChapters.length == 0) {
        var checkedChapters = ["0"]
    }

    return checkedChapters;
}

$(document).ready(function() {
    // Load the json file, push to var jsondrugs
    var jsondrugs = (function() {
        var jsondrugs = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': "/drugs.json",
            'dataType': "json",
            'success': function(data) {
                jsondrugs = data;
            }
        });
        return jsondrugs;
    })();
    // Convert drug list from Object Literal to Array
    var jsonarr = [];
    for (prop in jsondrugs) {
        jsonarr.push(jsondrugs[prop]);
    }


    randomCard(jsonarr, getCheckedChapters() );

    // Randomize a new drug on click
    $("#main-drugs").on('click', ".drug-close", function(e) {
        $("#main-drugs").html("");
        randomCard(jsonarr, getCheckedChapters() );
    });

    // Show the info
    $("#main-drugs").on('click', ".drug-hide", function(e) {
        e.preventDefault();
        var slug = $(this).attr("data-slug")
        $("#" + slug + " .drug-info").slideToggle();
    });

    // Spacebar magic: Show info or load new drug
    $('body').keyup(function(e) {
        if (e.keyCode == 32) {
            // user has pressed space
            if ($(".drug-info").is(":visible") || $(".drug-close").length == 0) {
                $("#main-drugs").html("");
                randomCard(jsonarr, getCheckedChapters() );
            } else {
                $(".drug-hide").click();
            }
        }
    });

    // Filtrering!
    // Sorter liste over kapitler genereret af Hugo.
    var $wrapper = $('.filter-checkbox-container');
    $wrapper.find('.filter-checkbox').sort(function(a, b) {
            return +a.dataset.chapter - +b.dataset.chapter;
        })
        .appendTo($wrapper);

    $(".filter-chapter").click(function() {
        var chap = $(this).data("chapter");
        getCheckedChapters();
    });

    $(".filter-chapter-all").click(function() {
        $(".filter-chapter").prop("checked", true);
        getCheckedChapters();
    });

    $(".filter-chapter-none").click(function() {
        $(".filter-chapter").prop("checked", false);
        getCheckedChapters();
    });
})
