/**
 * Function sortDivs: Sorts the dynamically loaded divs (alphabetically).
 * @param  {[type]} parent [The selector for the parent element in which the children should be sorted]
 * @param  {[type]} els    [The selector for the child elements to be sorted]
 * @return {[type]}        [Return the sorted els]
 */
var sortEls = function(parent, els) {
    $(parent + " > " + els).sort(dec_sort).appendTo(parent);

    function dec_sort(a, b) {
        return ($(b).data("slug").toUpperCase()) < ($(a).data("slug").toUpperCase()) ? 1 : -1;
    }
};

/** Function loadDrug: Loads the html file for the drug when clicked in menu.
 * [function description]
 * @param  {[type]} drug [description]
 * @return {[type]}      [description]
 */
var loadDrug = function(drug) {
    $.get("/drug/" + drug.attr("data-slug") + ".html", function(data) {
        $(data).prependTo("#main-drugs").slideDown();
        sortEls("#main-drugs", "div.info");
        // Scroll !
        $('html,body').animate({
            scrollTop: $("#" + drug.attr("data-slug")).offset().top
        });

        var li = "<li class=\"toc\" id=\"toc-" + drug.data("slug") + "\"data-slug=\"" + drug.attr("data-slug") + "\"><a href=\"#" + drug.attr("data-slug") + "\"> " + drug.attr("data-name") + "</a></li>";
        $(li).appendTo(".table-of-contents").slideDown();
        sortEls(".table-of-contents", "li:not(\"#toc-open\")");

        _paq.push(['trackEvent', 'Drug', 'Open', drug.attr("data-name"), 1]);
    });

};

// Function deleteDrug: Deletes the DOM element for the given drug.
var deleteDrug = function(drug) {
    $("#main-drugs #" + drug.attr("data-slug")).slideUp(function() {
        $(this).remove();
    });
    $(".table-of-contents #toc-" + drug.data("slug")).slideUp(function() {
        $(this).remove();
    })

    _paq.push(['trackEvent', 'Drug', 'Close', drug.attr("data-name"), -1]);
};


// Document ready!
$(document).ready(function() {
    $('.button-collapse').sideNav({
        menuWidth: 300, // Default is 240
        edge: 'left', // Choose the horizontal origin
        closeOnClick: false // Closes side-nav on <a> clicks, useful for Angular/Meteor
    });


    // Sorter stoflisten efter slug.


    $('.scrollspy').scrollSpy();

    // The navigation: Depending on if the drug exists in the DOM either delete
    // or load it in.
    $(".drug-list li a").click(function(e) {
        e.preventDefault();
        var drug = $(this);

        // If the drug exists:
        if ($("#main-drugs #" + drug.attr("data-slug")).length) {
            deleteDrug(drug);
        } else {
            loadDrug(drug);
        }
    });

    // Info card: Close and hide.
    $("#main-drugs").on('click', ".drug-close", function(e) {
        e.preventDefault();
        var slug = $(this).attr("data-slug")
        deleteDrug($("#" + slug));
    });


    $("#main-drugs").on('click', ".drug-hide", function(e) {
        e.preventDefault();
        var slug = $(this).attr("data-slug")
        $("#" + slug + " .drug-info").slideToggle();
    });

    // Delete welcome
    $(".welcome-close").click(function(e) {
        e.preventDefault();
        $("#welcome").slideUp(function() {
            $(this).remove();
        })
    })


    // Filtrering!
    // Sorter liste over kapitler genereret af Hugo.
    var $wrapper = $('.filter-checkbox-container');
    $wrapper.find('.filter-checkbox').sort(function(a, b) {
        return +a.dataset.chapter - +b.dataset.chapter;
    })
    .appendTo($wrapper);

    $(".filter-chapter").click(function() {
        var chap = $(this).data("chapter");
        $(".kapitel-" + chap).toggle();
    });

    $(".filter-chapter-all").click(function() {
        $(".filter-chapter").prop("checked", true);
        $(".stof").show();
    });

    $(".filter-chapter-none").click(function() {
        $(".filter-chapter").prop("checked", false);
        $(".stof").hide();
    });
    // Load the json file, push to var jsondrugs
    var jsondrugs = (function() {
        var jsondrugs = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': "/drugs.json",
            'dataType': "json",
            'success': function(data) {
                jsondrugs = data;
            }
        });
        return jsondrugs;
    })();
    // Convert drug list from Object Literal to Array
    var jsonarr = [];
    for (prop in jsondrugs) {
        jsonarr.push(jsondrugs[prop]);
    }

    // Søgning
    var searchInput;
    var searchoptions = {
        data: jsonarr,

        getValue: "name",

        list: {
            match: {
                enabled: true
            },
            onLoadEvent: function() {
                searchInput = $("#drug-search").val();
            },
            onChooseEvent: function() {
                var value = $("#drug-search").getSelectedItemData().slug;
                var navlink = $("#nav-" + value);
                if (navlink.hasClass("no-info")) {
                    alert("Beklager – ingen noter til dette stof.")
                } else {
                    navlink.click();
                }
                $("#drug-search").val(searchInput);
            }
        },
        placeholder: "Søg på stof i alle kapitler"
    };

    $("#drug-search").easyAutocomplete(searchoptions);

    $('form input').on('keypress', function(e) {
        return e.which !== 13;
    });

    /* Table of contents
    $(window).scroll(function() {
      var scrollTop = $(window).scrollTop();
      $(".table-of-contents").animate({
        top: scrollTop
      }, 0);
      });
*/
})
